﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace ConsoleApp2
{
    class CircularLinkedList<T> : IEnumerable<T>, IAlgorithm<T> where T : IComparable
    {
        Node<T> head;
        Node<T> tail;
        int count;
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            //if list is empty
            if (head == null)
            {
                head = node;
                tail = node;
                tail.Next = head;
            }
            else
            {
                node.Next = head;
                tail.Next = node;
                tail = node;
            }
            count++;
        }
        public bool Remove(T data)
        {
            Node<T> current = head;
            Node<T> previous = null;

            if (IsEmpty) return false;

            do
            {
                if (current.Data.Equals(data))
                {
                    //if Node is in the middle or in the end.
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current == tail)
                            tail = previous;
                    }
                    else
                    {
                        if (count == 1)
                        {
                            head = tail = null;
                        }
                        else
                        {
                            head = current.Next;
                            tail.Next = current.Next;
                        }
                    }
                    count--;
                    return true;
                }
                previous = current;
                current = current.Next;
            }
            while (current != head);
            return false;
        }
        public int Count { get { return count; } }
        public bool IsEmpty { get { return count == 0; } }
        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }
        public bool Contains(T data)
        {
            Node<T> current = head;
            if (current == null) return false;
            do
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            while (current != head);
            return false;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != head);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        public bool IsSorted()
        {
            Node<T> current = head;

            while (current != tail)
            {
                if (current.Data.CompareTo(current.Next.Data) == 1) return false;
                current = current.Next;
            }
            return true;
        }

        public struct TSortStackItem
        {
            public int Level;
            public Node<T> Item;
        }
        public void Sort()
        {
            Node<T> IntersectSorted(Node<T> pList1, Node<T> pList2)
            {
                Node<T> pCurItem, listResult;
                Node<T> p1, p2;
                p1 = pList1;
                p2 = pList2;
                if ((p1.Data.CompareTo(p2.Data) == -1) || (p1.Data.CompareTo(p2.Data) == 0))
                {
                    pCurItem = p1;
                    p1 = p1.Next;
                }
                else
                {
                    pCurItem = p2;
                    p2 = p2.Next;
                }
                listResult = pCurItem;
                while ((p1 != null) && (p2 != null))
                {
                    if ((p1.Data.CompareTo(p2.Data) == -1) || (p1.Data.CompareTo(p2.Data) == 0))//if (p1.Data <= p2.Data)
                    {
                        pCurItem.Next = p1;
                        pCurItem = p1;
                        p1 = p1.Next;
                    }
                    else
                    {
                        pCurItem.Next = p2;
                        pCurItem = p2;
                        p2 = p2.Next;
                    }
                }
                if (p1 != null)
                    pCurItem.Next = p1;
                else
                    pCurItem.Next = p2;
                return listResult;
            };

            TSortStackItem[] Stack = new TSortStackItem[31];
            int StackPos;
            Node<T> p;
            StackPos = 0;
            p = this.head;

            do
            {
                Stack[StackPos].Level = 1;
                Stack[StackPos].Item = p;
                p = p.Next;
                Stack[StackPos].Item.Next = null;
                ++StackPos;
                while ((StackPos > 1) && (Stack[StackPos - 1].Level == Stack[StackPos - 2].Level))
                {
                    Stack[StackPos - 2].Item = IntersectSorted(Stack[StackPos - 2].Item, Stack[StackPos - 1].Item);
                    ++(Stack[StackPos - 2].Level);
                    --StackPos;
                }
            } while (p != head);

            while (StackPos > 1)
            {
                Stack[StackPos - 2].Item = IntersectSorted(Stack[StackPos - 2].Item, Stack[StackPos - 1].Item);
                ++Stack[StackPos - 2].Level;
                --StackPos;
            }
            if (StackPos > 0)
                head = Stack[0].Item;

            if (head != null) //Being sorted list was disconnected, connect it.
            {
                Node<T> current = head;
                while (current.Next != null) current = current.Next;
                tail = current;
                current.Next = head;
            }

        }

        public int BinarySearch(Node<T> value)
        {
            //int cntNodes = this.cntNodes();
            Random random = new Random();
            if (!this.IsSorted())
                return random.Next(0, Count) * (-1) - 1;
            if (head.Data.CompareTo(value.Data) > 1)
                return 0;
            if (value.Data.CompareTo(tail.Data) > 1)
                return (~Count);
            Node<T> p = head;
            int cnt = 0;
            while (p != null)
            {
                if (p.Data.CompareTo(value.Data) == 0) return cnt;
                if (p.Next.Data.CompareTo(value.Data) == 1) return ~(cnt + 1);
                cnt++;
                if (p.Data.CompareTo(tail.Data) == 0) break;
                p = p.Next;
            }
            return (~Count);
        }
    }
}
