﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    interface IAlgorithm<T> where T : IComparable
    {
        void Sort();

        int BinarySearch(Node<T> value);
    }
}
