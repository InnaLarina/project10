﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is a CircularLinkedList:");
            Console.WriteLine("");
            CircularLinkedList<int> list = new CircularLinkedList<int>();
            Random random = new Random();
            for (int x = 0; x < 10; x++)
            {
                list.Add(random.Next(0, 9));
            }
            foreach (int i in list)
            {
                Console.Write(i);
            }

            Console.WriteLine("");
            Console.WriteLine("");

            if (list.IsSorted())
                Console.WriteLine("The CircularLinkedList is sorted.");
            else
                Console.WriteLine("The CircularLinkedList is not sorted.");

            Console.WriteLine("");

            list.Sort();

            Console.WriteLine("Now the CircularLinkedList is sorted:");
            Console.WriteLine("");

            foreach (int i in list)
            {
                Console.Write(i);
            }

            Console.WriteLine("");
            Console.WriteLine("");

            Console.WriteLine($"2 is on the place {list.BinarySearch(new Node<int>(2))}");
            Console.WriteLine($"5 is on the place {list.BinarySearch(new Node<int>(5))}");
            Console.WriteLine($"8 is on the place {list.BinarySearch(new Node<int>(8))}");

            Console.WriteLine("\nDone");
        }
    }
}
